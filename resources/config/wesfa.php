<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Whatsapp Configuration
    |--------------------------------------------------------------------------
    |
    | This block is for storing the credentials for whatsapp service.
    | This file provides the de facto location for this type of information,
    | allowing packages to have a conventional file to locate
    | the various service credentials.
    |
    */

    'whatsapp' => [
        'server_gateway' => env('WA_GATEWAY'),
        'registered_number' => env('WA_NUMBER')
    ],

    /*
    |--------------------------------------------------------------------------
    | One Time Password & Forgot Password Code Configuration
    |--------------------------------------------------------------------------
    */

    'common' => [

        /*
        |
        | This block is for storing how many auth model used on this project.
        | model can be store by string, example if your name modes is "Users.php"
        | you can type only "users" without extension. Then add the unique column
        | used on this model, example "id", "number", "email", "app_name", etc.
        | The schema will be like this :
        |
        | 'auth_model' => [
        |       'users' => ['id', 'email'],
        |       'customer => ['id', 'number_phone']
        | ]
        |
        */
        'auth_model' => [],

        /*
        |
        | This is configuration channel who used to send a otp.
        | You can 2 channel to send code, "email" or and "whatsapp".
        |
        */
        'channel' => [],
    ],

];
