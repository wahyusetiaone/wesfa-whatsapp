/*
|--------------------------------------------------------------------------
| Whatsapp Configuration
|--------------------------------------------------------------------------
|
| This block is for storing the credentials for whatsapp service.
| This file provides the de facto location for this type of information,
| allowing packages to have a conventional file to locate
| the various service credentials.
|
*/

    'whatsapp' => [
        'server_gateway' => env('WA_GATEWAY'),
        'registered_number' => env('WA_NUMBER')
    ],
