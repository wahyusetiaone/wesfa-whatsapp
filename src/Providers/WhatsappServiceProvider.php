<?php

namespace WESFA\Whatsapp;
use Illuminate\Support\ServiceProvider;

class WhatsappServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (config()->has('wesfa')) {
            if (config()->has('wesfa.whatsapp')){
                $old_config = $this->app['config']->get('wesfa.whatsapp', []);
                if ($old_config != require __DIR__.'/../../config/extra.php'){
                    $this->mergeConfigFrom(__DIR__.'/../../config/extra.php', 'wesfa');
                }
            }
        } else {
            $this->publishes([
                __DIR__.'/../../resources/config/wesfa.php' => config_path('wesfa.php'),
            ]);
        }

        $this->app->bind('whatsapp',function(){
            return new Whatsapp();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
