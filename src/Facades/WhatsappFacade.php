<?php


namespace WESFA\Whatsapp;


use Illuminate\Support\Facades\Facade;

class WhatsappFacade extends Facade {
    protected static function getFacadeAccessor() { return 'whatsapp'; }
}
