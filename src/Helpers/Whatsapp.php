<?php


namespace WESFA\Whatsapp;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Whatsapp
{
    public function checkNumberIsWhatsapp($number_phone)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . env('NUMBER_PHONE_EXPRESS') . '/is-whatsapp', [
            'number_phone' => $number_phone
        ]);
        if ($response->serverError()) {
            return ['error_status' => 'Timeout 30001'];
        }
        $data = $response->json('results.data');

        return $data;
    }

    public function getState($number_phone)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->get(env('URL_EXPRESS') . '/api/' . $number_phone . '/state');

        return $response->json();
    }

    public function getSession($number_phone)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->get(env('URL_EXPRESS') . '/api/' . $number_phone);

        return $response->json();
    }

    public function sendTextMessage($number_phone, $to_number_phone, $text)
    {
        try {
            $response = Http::timeout(86400)->connectTimeout(86400)->timeout(86400)->withHeaders([
                'Accept' => 'application/json',
            ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-text-message', [
                'to' => $to_number_phone,
                'message' => $text
            ]);

        } catch (\Exception $ee) {
            return [
                'code' => 200,
                'status' => 'failed',
                'message' => 'Empty Response from Server 52',
                'results' => []
            ];
        }

        if ($response->serverError()) {
            return [
                'code' => 200,
                'status' => 'failed',
                'message' => 'Server Timeout',
                'results' => []
            ];
        }

        return $response->json();
    }

    public function sendLinkMessage($number_phone, $to_number_phone, $text, $nameUrl, $url, $footer)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-link-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'nameUrl' => $nameUrl,
            'url' => $url,
            'footer' => $footer
        ]);

        return $response->json();
    }

    public function sendCallLinkMessage($number_phone, $to_number_phone, $text, $nameUrl, $url, $nameCall, $call, $footer)
    {
        $params = [
            'to' => $to_number_phone,
            'message' => $text,
            'nameUrl' => $nameUrl,
            'url' => $url,
            'nameCall' => $nameCall,
            'call' => (int)$call,
            'footer' => $footer
        ];

        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-link-n-call-message', $params);

        return $response->json();
    }

    public function sendImageMessage($number_phone, $to_number_phone, $text, $image)
    {
        $file = Storage::disk('message')->put('image', $image);

        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-image-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'image' => Storage::disk('message')->url($file)
        ]);

        return $response->json();
    }

    public function sendLinkImageMessage($number_phone, $to_number_phone, $text, $link)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-image-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'image' => $link
        ]);

        return $response->json();
    }

    public function sendDocMessage($number_phone, $to_number_phone, $text, $file)
    {
        $file = Storage::disk('document')->put('document', $file);

        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-document-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'doc' => Storage::disk('document')->url($file)
        ]);

        return $response->json();
    }


    public function readTextMessage($number_phone, $remoteJid, $fromMe, $id)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/read-message', [
            'key' => [
                "remoteJid" => $remoteJid,
                "fromMe" => $fromMe,
                "id" => $id
            ]
        ]);

        return $response->json();
    }

    public function sendVerifyLinkMessage($to_number_phone)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . env('NUMBER_PHONE_EXPRESS') . '/send-text-message', [
            'to' => $to_number_phone,
            'message' => 'to verify your whatsapp number please this link : ' . route('verify-whatsapp-number', $this->base64url_encode($to_number_phone))
        ]);

        return $response->json();
    }

    public function sendOneTimePassword($to_number_phone)
    {
        $six_digit_random_number = random_int(100000, 999999);

        $customer = Customer::find(auth()->id());
        $customer->otp()->delete();
        $customer->otp()->firstOrCreate([
            'code' => $six_digit_random_number
        ]);

        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . env('NUMBER_PHONE_EXPRESS') . '/send-text-message', [
            'to' => $to_number_phone,
            'message' => 'You\'r code OTP : *' . $six_digit_random_number . '* , this code will expired in 1 minutes.'
        ]);

        return $response->json();
    }

    public function sendForgotPasswordCode($to_number_phone, $identity = null)
    {
        $six_digit_random_number = random_int(100000, 999999);
        if (config('wesfa.whatsapp.extra_identity')){
            $customer = Customer::where('number_phone', $to_number_phone)->where(config('wesfa.whatsapp.extra_identity'), $identity)->first();
        }else{
            $customer = Customer::where('number_phone', $to_number_phone)->first();
        }
        $customer->fpc()->delete();
        $customer->fpc()->firstOrCreate([
            'code' => $six_digit_random_number,
        ]);

        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . env('NUMBER_PHONE_EXPRESS') . '/send-text-message', [
            'to' => $to_number_phone,
            'message' => 'You\'r code to Reset Password : *' . $six_digit_random_number . '* , this code will expired in 1 minutes.'
        ]);

        return $response->json();
    }


    public function scanDevice($number_phone)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->get(env('URL_EXPRESS') . '/api/' . $number_phone . '/scan-device');

        return $response->json();
    }

    function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    public function sendScheduleTextMessage($number_phone, $to_number_phone, $text)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-text-message', [
            'to' => $to_number_phone,
            'message' => $text
        ]);

        return $response->json();
    }

    public function sendScheduleLinkMessage($number_phone, $to_number_phone, $text, $nameUrl, $url, $footer)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-link-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'nameUrl' => $nameUrl,
            'url' => $url,
            'footer' => $footer
        ]);

        return $response->json();
    }

    public function sendScheduleCallLinkMessage($number_phone, $to_number_phone, $text, $nameUrl, $url, $nameCall, $call, $footer)
    {
        $params = [
            'to' => $to_number_phone,
            'message' => $text,
            'nameUrl' => $nameUrl,
            'url' => $url,
            'nameCall' => $nameCall,
            'call' => $call,
            'footer' => $footer
        ];

        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-link-n-call-message', $params);

        return $response->json();
    }

    public function sendScheduleImageMessage($number_phone, $to_number_phone, $text, $image)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-image-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'image' => $image
        ]);

        return $response->json();
    }

    public function sendScheduleLinkImageMessage($number_phone, $to_number_phone, $text, $link)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-image-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'image' => $link
        ]);

        return $response->json();
    }

    public function sendScheduleDocMessage($number_phone, $to_number_phone, $text, $file)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . $number_phone . '/send-document-message', [
            'to' => $to_number_phone,
            'message' => $text,
            'doc' => $file
        ]);

        return $response->json();
    }

    public function sendNotification($to_number_phone, $text)
    {
        $response = Http::timeout(86400)->connectTimeout(86400)->withoutVerifying()->withHeaders([
            'Accept' => 'application/json',
        ])->post(env('URL_EXPRESS') . '/api/' . env('NUMBER_PHONE_EXPRESS') . '/send-text-message', [
            'to' => $to_number_phone,
            'message' => $text
        ]);

        return $response->json();
    }
}
